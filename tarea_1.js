/*
338900 - Marley Zaragoza Balderrama

1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json
    C:\Program Files\MongoDB\Tools\100\bin>mongoimport -d students -c grades < C:\data\grades.json
2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?
    test> use students
    switched to db students
    students> db.grades.count()
    DeprecationWarning: Collection.count() is deprecated. Use countDocuments or estimatedDocumentCount.
    800
3) Encuentra todas las calificaciones del estudiante con el id numero 4.
    students> db.grades.find({"student_id": 4}, {"score": 1})
    [
    {
        _id: ObjectId("50906d7fa3c412bb040eb588"),
        score: 27.29006335059361
    },
    {
        _id: ObjectId("50906d7fa3c412bb040eb587"),
        score: 87.89071881934647
    },
    {
        _id: ObjectId("50906d7fa3c412bb040eb589"),
        score: 5.244452510818443
    },
    { _id: ObjectId("50906d7fa3c412bb040eb58a"), score: 28.656451042441 }
    ]
4) ¿Cuántos registros hay de tipo exam?
    students> db.grades.count({"type": "exam"})
    200
5) ¿Cuántos registros hay de tipo homework?
    students> db.grades.count({"type": "homework"})
    400
6) ¿Cuántos registros hay de tipo quiz?
    students> db.grades.count({"type": "quiz"})
    200
7) Elimina todas las calificaciones del estudiante con el id numero 3
    students> db.grades.update({"student_id": 3}, {$unset: {score:1}}, {multi:true})
    DeprecationWarning: Collection.update() is deprecated. Use updateOne, updateMany, or bulkWrite.
    {
    acknowledged: true,
    insertedId: null,
    matchedCount: 4,
    modifiedCount: 4,
    upsertedCount: 0
    }
    students> db.grades.find({"student_id": 3}, {"score": 1})
    [
    { _id: ObjectId("50906d7fa3c412bb040eb584") },
    { _id: ObjectId("50906d7fa3c412bb040eb585") },
    { _id: ObjectId("50906d7fa3c412bb040eb583") },
    { _id: ObjectId("50906d7fa3c412bb040eb586") }
    ]
8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
    students> db.grades.find({"type": "homework", "score": 75.29561445722392}, {"student_id": 1})
    [ { _id: ObjectId("50906d7fa3c412bb040eb59e"), student_id: 9 } ]
9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
    students> db.grades.update({"_id": ObjectId('50906d7fa3c412bb040eb591')}, {$set: {"score": 100}}, {multi: true})
    {
    acknowledged: true,
    insertedId: null,
    matchedCount: 1,
    modifiedCount: 1,
    upsertedCount: 0
    }
    students> db.grades.find({"_id": ObjectId('50906d7fa3c412bb040eb591')}, {"score": 1})
    [ { _id: ObjectId("50906d7fa3c412bb040eb591"), score: 100 } ]
10) A qué estudiante pertenece esta calificación.
    students> db.grades.find({"_id": ObjectId('50906d7fa3c412bb040eb591')}, {"student_id": 1})
    [ { _id: ObjectId("50906d7fa3c412bb040eb591"), student_id: 6 } ]
*/